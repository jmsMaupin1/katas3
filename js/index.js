const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

var katas = {
    // 1
    oneThroughTwenty: function () {
        const numbers = [];
        for (let i = 1; i < 21; i++) numbers.push(i);

        return numbers;
    },

    // 2
    evensToTwenty: function () {
        const numbers = []
        for (let i = 2; i < 21; i += 2) numbers.push(i);

        return numbers;
    },

    // 3
    oddsToTwenty: function () {
        const numbers = []
        for (let i = 1; i < 21; i += 2) numbers.push(i);

        return numbers;
    },

    // 4
    multiplesOfFive: function () {
        const numbers = []
        for (let i = 1; i < 21; i++) numbers.push(i * 5);

        return numbers;
    },

    // 5
    squareNumbers: function () {
        const numbers = []
        for (let i = 1; i < 11; i++) numbers.push(i * i);

        return numbers;
    },

    // 6
    countingBackwards: function () {
        const numbers = []
        for (let i = 20; i > 0; i--) numbers.push(i);

        return numbers;
    },

    // 7
    evenNumbersBackwards: function () {
        const numbers = []
        for (let i = 20; i > 0; i -= 2) numbers.push(i);

        return numbers;
    },

    // 8
    oddNumbersBackwards: function () {
        const numbers = []
        for (let i = 19; i > 0; i -= 2) numbers.push(i);

        return numbers;
    },

    // 9
    multiplesOfFiveBackwards: function () {
        const numbers = []
        for (let i = 20; i > 0; i--) numbers.push(i * 5);

        return numbers;
    },

    // 10
    squareNumbersBackwards: function () {
        const numbers = []
        for (let i = 10; i > 0; i--) numbers.push(i * i);

        return numbers;
    },

    // 11
    sampleArrayElements: function () {
        let elements = "";
        let template = p => `<p class="sa-ele">${p}</p>`;

        for (let i = 0; i < sampleArray.length; ++i)
            elements += template(sampleArray[i]);

        return elements;
    },

    // 12
    evenSampleArrayElements: function () {
        let elements = "";
        let template = p => `<p class="sa-ele">${p}</p>`;

        for (let i = 0; i < sampleArray.length; ++i)
            if (sampleArray[i] % 2 === 0)
                elements += template(sampleArray[i]);

        return elements;
    },

    // 13
    oddSampleArrayElements: function () {
        let elements = "";
        let template = p => `<p class="sa-ele">${p}</p>`;

        for (let i = 0; i < sampleArray.length; ++i)
            if (sampleArray[i] % 2 !== 0)
                elements += template(sampleArray[i]);

        return elements;
    },

    // 14
    squaredSampleArrayElements: function () {
        let elements = "";
        let template = p => `<p class="sa-ele">${p}</p>`;

        for (let i = 0; i < sampleArray.length; ++i)
            elements += template(sampleArray[i] * sampleArray[i]);

        return elements
    },

    // 15
    sumOneToTwenty: function () {
        let sum = 0;
        for (let i = 1; i < 21; ++i)
            sum += i;

        return `<p class="sa-ele">${sum}</p>`;
    },

    // 16
    sampleArraySumElements: function () {
        let sum = sampleArray.reduce((accum, cur) => {
            return accum + cur;
        }, 0);

        return `<p class="sa-ele">${sum}</p>`;
    },

    // 17
    smolSampleArrayElement: function () {
        let smol = sampleArray.reduce((accum, cur) => {
            if (cur < accum) return cur;
            else return accum;
        }, Number.MAX_VALUE);

        return `<p class="sa-ele">${smol}</p>`;
    },

    // 18
    thiccSampleArrayElement: function () {
        let thicc = sampleArray.reduce((accum, cur) => {
            if (cur > accum) return cur;
            else return accum;
        }, Number.MIN_SAFE_INTEGER);

        return `<p class="sa-ele">${thicc}</p>`;
    },

    // 19 - 23
    displayRects: function (height, width, color) {
        return `<div style="
            height: ${height}; 
            width: ${width}; 
            background:${color};
            margin: 5px;"></div>`;
    }
}


function appendTo(id, template, parentClass) {
    let child = document.createElement('div');
    if (parentClass) child.classList.add(parentClass);

    child.innerHTML = template;

    if (id === document.body)
        document.body.appendChild(child);
    else
        document.getElementById(id).appendChild(child);
}

let keys = Object.keys(katas);

for (let i = 0; i < keys.length - 1; i++) {
    let template =
        `<div id=${i}>
            <br/>
            <h1>Kata: ${i + 1}</h1> 
            </br>
            <div style="width: 100vw; ">${katas[keys[i]]()}</div>
        </div>`;
    appendTo(document.body, template);
}

for (let i = 0; i < 5; i++) {
    let template =
        `<div id=${i + 19}>
            <br/>
            <h1>Kata: ${i + 19}</h1> 
            </br>
            <div id="div-${i + 19}" style="display: flex; flex-wrap: wrap; width: 100vw;"></div>
        </div>`;

    appendTo(document.body, template);
}

// 19
for (let i = 0; i < 20; i++) {
    appendTo("div-19", katas.displayRects("20px", "100px", "grey"))
}

// 20
for(let i = 0; i < 20; i++) {
    appendTo("div-20", katas.displayRects("20px", `${105+i*5}px`, "grey"));
}

// 21
for(let i = 0; i < sampleArray.length; ++i) {
    appendTo("div-21", katas.displayRects("20px", `${sampleArray[i]}px`, "grey"))
}

// 22
for(let i = 0; i < sampleArray.length; ++i) {
    appendTo("div-22", katas.displayRects(
        "20px",
        `${sampleArray[i]}px`,
        `${i % 2 === 0 ? "red" : "grey"}`
    ))
}

// 23
for(let i = 0; i < sampleArray.length; ++i) {
    appendTo("div-23", katas.displayRects(
        "20px",
        `${sampleArray[i]}px`,
        `${sampleArray[i] % 2 === 0 ? "red" : "grey"}`
    ));
}